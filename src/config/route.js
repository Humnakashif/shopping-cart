import React from 'react';
import { BrowserRouter, Route,  Routes } from 'react-router-dom';
import Shop from '../views/Shop';
import '../assets/styles/style.css';

const Routing =()=>{
    return(
        <BrowserRouter>
            <Routes> 
                <Route exact path="/" element={<Shop/>}/>
            </Routes>
        </BrowserRouter>
    )
}
export default Routing