import React,{useState,useEffect} from 'react'
import API from '../API.js';
import ProductsList from '../components/Products.js';


const Shop = () =>{
    const [listing, setListing] = useState([]);

        useEffect(() => {
            API.get("benirvingplt/products/products").then(res => {
                setListing(res.data.map(v => ({...v, count: 1})));
        })
    },[])

    return(
        <div className="container" data-testid="counter"> 
            <ProductsList data={listing} list={listing} />
        </div>
    )
}



export default Shop