import React from "react";
import { Select } from 'antd';
const { Option } = Select;

const FilterBox =({changeColor})=>{
    
    const onSearch = (value) => {
    console.log('search:', value);
    };

    return(
            <Select
            showSearch
            placeholder="Select Color"
            optionFilterProp="children"
            size="large"
            style={{
                width: 300,
                marginBottom:5

              }}
            onChange={changeColor}
            onSearch={onSearch}
            filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
            <Option value="Black">Black</Option>
            <Option value="Stone">Stone</Option>
            <Option value="Red">Red</Option>
        </Select>

    )
}


export default FilterBox