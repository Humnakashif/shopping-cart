import React, {useState } from "react";
import {DeleteFilled } from '@ant-design/icons';
import { Tag, Button, Result,Input } from 'antd';
import FilterBox from "./FilterBox";

const ProductsList = ({list})=>{

      const [products, setProducts] = useState(list);
      const [color, setColor] = useState("");
      const [qty, setQty] = useState(1);
     
      const itemCount = !!products && products.reduce((quantity, product) => {
          return quantity + +product.count;
        }, 0);
        const subTotal = !!products && products.reduce((total, product) => {
          return total + product.price * +product.count;
        }, 0);
      
      const onChangeProductQuantity = (index, event) => {
        const value = event.target.value;
        const valueInt = parseInt(value);
        const cloneProducts = [...products];
        if (value === "") {
          cloneProducts[index].count = value;
        } else if (valueInt > 0 && valueInt < 100) {
          cloneProducts[index].count= valueInt;
        }
    
        setProducts(cloneProducts);
        setQty(valueInt)
        console.log(cloneProducts)
      };
    
      const onRemoveProduct = (i) => {
        const filteredProduct = products.filter((product, index) => {
          return index != i;
        });
    
        setProducts(filteredProduct);
      };

      const formatCurrency = (value)=>  {
          return Number(value).toLocaleString("en-US", {
            style: "currency",
            currency: "USD"
          });
        }
      const onChange = (value) => {
        console.log(`selected ${value}`);
        setColor(value)
        };
    
    return(
      <div >
        <header className="container">
            <h1>Shopping Cart</h1>
            <ul className="breadcrumb">
              <li>Home</li>
              <li>Shopping Cart</li>
            </ul>
            <FilterBox changeColor={onChange}/>
            <span className="count">{itemCount} items in the bag</span>
          </header>
        <div >
          {!!products && products.length > 0 ? (
            <div>
              <section className="container">
                <ul className="products">
                  {products.filter(items => items.colour.match(color)).map((product, index) => {
                    return (
                      <li className="row" key={index}>
                        <div className="col left">
                          <div className="thumbnail">
                            <a href="#">
                              <img src={product.img} alt={product.name}  style={{borderRadius:"4px", height:"147px", objectFit:"cover"}}/>
                            </a>
                          </div>
                          <div className="detail">
                            <div className="name">
                              <a href="#">{product.name}</a>
                            </div>
                            <div className="description">{product.colour}</div>
                            <div className="price"><Tag color="success">{formatCurrency(product.price)}</Tag> </div>
                          </div>
                        </div>
                        <div className="col right">
                          <div className="quantity" >
                            <input
                              type="text"
                              className="quantity"
                              step="1"
                              defaultValue={product.count}
                              onChange={(event) => onChangeProductQuantity(index, event)}
                            />
                          </div>
                          <div className="remove">
                            <Button type="danger" data-testid="removeBtn" onClick={() => onRemoveProduct(index)} icon={<DeleteFilled />} size={"large"}>
                              Remove
                            </Button>
                          </div>
                        </div>
                      </li>
                    );
                  })}
                </ul>
              </section>
              <section className="container">
                <div className="summary">
                  <ul>
                    <li>
                      Total <span> <b> {formatCurrency(subTotal)} </b></span>
                    </li>
                  </ul>
                </div>
              </section>
            </div>
          ) : (
              <Result
                status="404"
                title="Empty Cart"
                subTitle="There are no products in your cart."
                extra={<Button type="primary" onClick={() => setProducts(list)}>Back to Shop</Button>}
              />
          )}
        </div>
      </div>
    )
}

export default ProductsList

  
 
  